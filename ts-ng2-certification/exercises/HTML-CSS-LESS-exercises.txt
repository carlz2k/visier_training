(Refer to https://sites.google.com/a/visiercorp.com/developer-enablement/training/new-to-development/client-certification/html-css-and-less)

HTML/CSS
1. Consider the following HTML & CSS. Please answer what the color of each display text will be without actually running this code. Provide explanations for each answer.

Answer:

LESS
1. Write a mixin so you don’t need to type out the browser-specific tags each time you want to use the “flex” CSS property.
(Hint: browser-specific tags in this case are -ms-flex, -webkit-flex)

Answer:

Shadow DOM
1. Using the skills you learned in the reading, replicate this card for displaying your favorite shapes. The "content"
should be the shapes. Everything else should be a part of the template.
You may use this JSFiddle as a starting point: https://jsfiddle.net/0k2ys915/2/

Answer (provided in a JSFiddle):

Flexbox
1. Using this jsfiddle (http://jsfiddle.net/codyjrobinson/2cq38mhd/1/) replicate the following image by using flex-box.
(Do not change the HTML structure):

Answer (provided in a JSFiddle):
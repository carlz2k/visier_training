import {Component} from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common';

@Component({
    selector: 'v-page-component',
    templateUrl: 'modules/content/components/pageComponent.ng.html',
    directives: [CORE_DIRECTIVES],
})

export class PageComponent {
        constructor() {}
}

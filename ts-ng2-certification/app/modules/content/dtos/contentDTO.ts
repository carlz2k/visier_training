/**
 * Should not modify this file.
 */


export class ContentDTO {
    constructor (
        public contentID: number,
        public contentType: string,
        public displayName: string,
        public editable: boolean) {}


    public static fromJson(data: any): ContentDTO {
        return new ContentDTO(
            data.contentID,
            data.contentType,
            data.displayName,
            data.editable);
    }
}

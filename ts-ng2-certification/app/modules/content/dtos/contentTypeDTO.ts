/**
 * Should not modify this file.
 */

export class ContentTypeDTO {
    constructor (
        public contentType: string,
        public displayName: string
    ) {}

    public static fromJson(data: any): ContentTypeDTO {
        return new ContentTypeDTO(data.contentType, data.displayName);
    }
}

import {Http} from '@angular/http';
import {Injectable} from "@angular/core";
import {ContentDTO} from "../dtos/contentDTO";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import "rxjs/operator/map";
import {ContentTypeDTO} from "../dtos/contentTypeDTO";

/**
 * Should not modify this file.
 */

@Injectable()
export class ContentService {

    constructor(
        private _http: Http
    ) {}

    public getContents(): Observable<Array<ContentDTO>> {
        return this._http.get('assets/contents.json').map(res => {
            return res.json().map((data: any) => ContentDTO.fromJson(data));
        });
    }

    public getContentTypes(): Observable<Array<ContentTypeDTO>> {
        return this._http.get('assets/contentTypes.json').map(res => {
            return res.json().map((data: any) => ContentTypeDTO.fromJson(data));
        });
    }
}

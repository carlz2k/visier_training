import {Component} from '@angular/core';
import {PageComponent} from "./content/components/pageComponent";

@Component({
  selector: 'my-app',
  template: `
    <div>
        <v-page-component></v-page-component>
    </div>
  `,
  directives: [PageComponent]
})
export class AppComponent {
}

package com.visiercorp.src.main.scala

import org.scalatest.FunSpec

class BankAccountSummaryDTOTest extends FunSpec {
  it("Can be created from a specified name and balance.") {

  }

  describe("Created from a specified BankAccount") {

    it("Has name that is combined from the first and last name.") {

    }

    it("Has balance that is rounded up to 2 decimal places.") {

    }

    it("Has balance that is rounded down to 2 decimal places.") {

    }
  }

  it("Can be converted to JSON") {

  }

  it("Can be converted to JSON even with null name") {

  }
}

package com.visiercorp.src.main.scala

import com.visiercorp.src.main.scala.domain.models.TeslaModelS
import org.scalatest.FunSpec

class TeslaModelSTest extends FunSpec {

  describe("A Tesla Model S") {
    val modelS: TeslaModelS = new TeslaModelS

    it("has a Model S name") {
      assert(modelS.name === "Tesla Model S")
    }

    it("has a medium battery capacity") {
      assert(modelS.chargeCapacity === 10)
    }

    it("has a single motor") {

    }

    it("Can be upgraded with a large battery") {

    }

    it("Can be upgraded with a large battery and a dual motor") {

    }
  }
}
package com.visiercorp.src.main.scala

trait PerfTiming {
  /**
    * Times specific function a number of times and prints out the average time it takes to run the function.
    * @param numberOfTimingLoops The number of times to run the function.
    * @param functionToTime  The function run.
    */
  def time[A](numberOfTimingLoops: Int)(functionToTime: => A) = {
    if (numberOfTimingLoops < 1) {
      throw new Exception("numberOfTimingLoops must be at least 1")
    }

    var count: Int = numberOfTimingLoops
    var totalMillis: Long = 0
    while (count > 0) {
      val now: Long = System.currentTimeMillis
      functionToTime
      totalMillis += System.currentTimeMillis - now
      count -= 1
    }
    println("%d microseconds".format(totalMillis / numberOfTimingLoops))
  }
}

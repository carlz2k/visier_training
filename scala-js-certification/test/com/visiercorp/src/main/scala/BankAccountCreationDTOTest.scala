package com.visiercorp.src.main.scala

import org.scalatest.FunSpec

class BankAccountCreationDTOTest extends FunSpec {

  describe("A BankAccountCreationDTO") {
    it("Can be created from valid JSON") {

    }

    it("Cannot be created from invalid JSON") {

    }

    it("Can be used to create a BankAccount") {

    }
  }
}

package com.visiercorp.src.main.scala

import org.scalatest.FunSpec

class BadPerformingCode extends FunSpec with PerfTiming {
  val NUM_TIMING_LOOPS = 5
  val ITERATIONS = 25000000
  val ITERATION_RANGE = 0 until ITERATIONS
  val ITERATION_INT_SEQ: IndexedSeq[Int] = ITERATION_RANGE.toIndexedSeq
  val ITERATION_SEQ: IndexedSeq[Integer] = ITERATION_RANGE.map(i => Integer.valueOf(i))

  /**
    * @return default double value, possibly NaN
    */
  def defaultValue: Double = {
    var acc: Double = 0
    var i = 0
    while (i < 10) {
      acc += Math.random()
      i += 1
    }

    if (acc != 0) Double.NaN else 0
  }

  class Measure {
    var value = 0L

    def update(rowValue: Double): Unit = {
      if (!rowValue.isNaN) {
        value += 1
      }
    }
  }

  describe("Calculate measures for our data set!") {
    var result = 0L
    var observedRecordsPerMeasure: Array[Int] = Array[Int](0)
    time(NUM_TIMING_LOOPS) {
      val measure = new Measure()
      val measures = IndexedSeq(measure)
      val observedRecords: Array[Int] = new Array[Int](measures.length)

      ITERATION_INT_SEQ.foreach { rowIdx =>
        measures.zipWithIndex.foreach { measureP =>
          measureP._1.update(
            {
              if (rowIdx % 2 == 0) defaultValue else rowIdx.toDouble
            }
          )
          observedRecords(measureP._2) += 1
        }
      }

      result = measure.value
      observedRecordsPerMeasure = observedRecords
    }

    println(result, observedRecordsPerMeasure.mkString(":"))
  }
}

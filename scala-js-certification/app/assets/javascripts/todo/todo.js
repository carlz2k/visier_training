define([
    "jquery",
    "backbone",
    "knockout",
    "text!templates/todo.ko.html"
], function(
    $,
    Backbone,
    ko,
    todoTemplate
) {
    return function() {
        $('body').append(todoTemplate);
    };
});
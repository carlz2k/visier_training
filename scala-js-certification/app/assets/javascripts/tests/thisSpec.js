define([
    "module",
    "exercises/this"
], function(
    self,
    thisExercise
) {
    return function() {
        module(self.id);
    };
});
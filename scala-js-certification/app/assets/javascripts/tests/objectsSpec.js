define([
    "module",
    "exercises/objects"
], function(
    self,
    objects
) {
    return function() {
        module(self.id);
    };
});
define([
    "module",
    "exercises/generalQuestions"
], function(
    self,
    generalQuestions
) {
    return function() {
        module(self.id);
    };
});
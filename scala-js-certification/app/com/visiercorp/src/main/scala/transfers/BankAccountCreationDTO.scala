package com.visiercorp.src.main.scala.transfers

import com.visiercorp.src.main.scala.domain.models.BankAccount

/**
 * The DTO used for bank account creation.
 * @param firstName The bank account owner's first name.
 * @param lastName The bank account owner's last name.
 * @param balance The initial bank account balance.
 */
case class BankAccountCreationDTO(firstName: String, lastName: String, balance: Double) {

  /**
   * Returns the server-side domain representation of the BankAccountCreationDTO.
   * @return BankAccount.
   */
  def toBankAccount(): BankAccount = ???
}

object BankAccountCreationDTO {
  import play.api.libs.json._

  private lazy val jsonReader : Reads[BankAccountCreationDTO] = ???

  /**
   * Converts specified JSON into BankAccountCreationDTO.  Throws exception if JSON cannot be converted.
   * @param json  The JsValue to convert.
   * @return BankAccountCreationDTO.
   */
  def fromJSON(json: JsValue) : BankAccountCreationDTO = ???
}

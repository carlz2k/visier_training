package com.visiercorp.src.main.scala.domain.models

class TeslaModelS extends Car with MediumBattery {
  override val name: String = "Tesla Model S"
}

package com.visiercorp.src.main.scala.domain.models

trait Chargeable {
  self: Car =>

  def chargeCapacity: Double
}
